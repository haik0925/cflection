#ifndef CFLECTION_H
#define CFLECTION_H
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#define cfl_array_count(arr) (sizeof(arr) / sizeof((arr)[0]))
#define cfl_assert(exp) assert(exp)

typedef enum
{
    CflTokenType_Undefined = 0,
    CflTokenType_Chars,
    CflTokenType_Numbers,
    CflTokenType_Whitespaces,
    CflTokenType_OpenParenthesis,
    CflTokenType_ClosedParenthesis,
    CflTokenType_OpenBrace,
    CflTokenType_ClosedBrace,
    CflTokenType_OpenBracket,
    CflTokenType_ClosedBracket,
    CflTokenType_Semicolon,
    CflTokenType_EqualSign,
    CflTokenType_Dot,
    CflTokenType_EOF,
} CflTokenType;

typedef struct
{
    CflTokenType type;
    const char* begin;
    int length;
} CflToken;

typedef struct
{
    CflToken* tokens;
} CflTokenBuffer;

int* cfl__token_buffer_size_ptr(CflTokenBuffer* buffer)
{
    return buffer->tokens == NULL ? NULL : (int*)buffer->tokens - 1;
}

int* cfl__token_buffer_capacity_ptr(CflTokenBuffer* buffer)
{
    return buffer->tokens == NULL ? NULL : (int*)buffer->tokens - 2;
}

int cfl_token_buffer_size(CflTokenBuffer* buffer)
{
    int* ptr = cfl__token_buffer_size_ptr(buffer);
    return ptr == NULL ? 0 : *ptr;
}

int cfl_token_buffer_capacity(CflTokenBuffer* buffer)
{
    int* ptr = cfl__token_buffer_capacity_ptr(buffer);
    return ptr == NULL ? 0 : *ptr;
}

void cfl_token_buffer_free(CflTokenBuffer* buffer)
{
    void* mem = cfl__token_buffer_capacity_ptr(buffer);
    free(mem);
    buffer->tokens = NULL;
}

void cfl_token_buffer_add(CflTokenBuffer* buffer, CflToken value)
{
    // Resize if needed.
    {
        const int initial_buffer_capacity = 8;
        int capacity = cfl_token_buffer_capacity(buffer);
        int size = cfl_token_buffer_size(buffer);
        if (size >= capacity)
        {
            int new_capacity = (capacity == 0) ? initial_buffer_capacity : capacity * 2;
            int new_memory_size = sizeof(int) * 2 + sizeof(*buffer->tokens) * new_capacity;
            void* mem = malloc(new_memory_size);
            CflTokenBuffer new_buffer = {.tokens = (CflToken*)((int*)mem + 2)};
            *cfl__token_buffer_capacity_ptr(&new_buffer) = new_capacity;
            *cfl__token_buffer_size_ptr(&new_buffer) = size;
            if (buffer->tokens != NULL)
            {
                memcpy(new_buffer.tokens, buffer->tokens, size * sizeof(*buffer->tokens));
            }
            cfl_token_buffer_free(buffer);
            buffer->tokens = new_buffer.tokens;
            int a = 0;
        }
    }

    {
        int* size_ptr = cfl__token_buffer_size_ptr(buffer);
        buffer->tokens[*size_ptr] = value;
        ++(*size_ptr);
    }
}

bool cfl_is_whitespace(char ch)
{
    bool is_whitespace = (ch == ' ') || (ch == '\t') || (ch == '\n');
    return is_whitespace;
}

bool cfl_is_alphabet(char ch)
{
    bool is_alphabet = ((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z'));
    return is_alphabet;
}

bool cfl_is_number(char ch)
{
    bool is_number = (ch >= '0') && (ch <= '9');
    return is_number;
}

void cfl__log(CflToken token)
{
    char* printbuf = malloc(sizeof(*token.begin) * (token.length + 1));
    strncpy(printbuf, token.begin, token.length);
    printbuf[token.length] = '\0';

    switch (token.type)
    {
    case CflTokenType_Undefined:
        printf("CflTokenType_Undefined: %s", printbuf);
        break;
    case CflTokenType_Chars:
        printf("CflTokenType_Chars: %s", printbuf);
        break;
    case CflTokenType_Numbers:
        printf("CflTokenType_Numbers: %s", printbuf);
        break;
    case CflTokenType_Whitespaces:
        printf("CflTokenType_Whitespaces: %s", printbuf);
        break;
    case CflTokenType_OpenParenthesis:
        printf("CflTokenType_OpenParenthesis: %s", printbuf);
        break;
    case CflTokenType_ClosedParenthesis:
        printf("CflTokenType_ClosedParenthesis: %s", printbuf);
        break;
    case CflTokenType_OpenBrace:
        printf("CflTokenType_OpenBrace: %s", printbuf);
        break;
    case CflTokenType_ClosedBrace:
        printf("CflTokenType_ClosedBrace: %s", printbuf);
        break;
    case CflTokenType_OpenBracket:
        printf("CflTokenType_OpenBracket: %s", printbuf);
        break;
    case CflTokenType_ClosedBracket:
        printf("CflTokenType_ClosedBracket: %s", printbuf);
        break;
    case CflTokenType_Semicolon:
        printf("CflTokenType_Semicolon: %s", printbuf);
        break;
    case CflTokenType_EqualSign:
        printf("CflTokenType_EqualSign: %s", printbuf);
        break;
    case CflTokenType_Dot:
        printf("CflTokenType_Dot: %s", printbuf);
        break;
    default:
        printf("ERROR");
    }
    printf("\n");

    free(printbuf);
}

CflTokenBuffer cfl_tokenize(const char* source)
{
    CflTokenBuffer result = {0};

    const char* ch = source;
    while (*ch != '\0')
    {
        CflToken token = {.begin = ch};
        switch (*ch)
        {
        case '{':
            token.type = CflTokenType_OpenBrace;
            token.length = 1;
            ++ch;
            break;
        case '}':
            token.type = CflTokenType_ClosedBrace;
            token.length = 1;
            ++ch;
            break;
        case '(':
            token.type = CflTokenType_OpenParenthesis;
            token.length = 1;
            ++ch;
            break;
        case ')':
            token.type = CflTokenType_ClosedParenthesis;
            token.length = 1;
            ++ch;
            break;
        case '[':
            token.type = CflTokenType_OpenBracket;
            token.length = 1;
            ++ch;
            break;
        case ']':
            token.type = CflTokenType_ClosedBracket;
            token.length = 1;
            ++ch;
            break;
        case ';':
            token.type = CflTokenType_Semicolon;
            token.length = 1;
            ++ch;
            break;
        case '=':
            token.type = CflTokenType_EqualSign;
            token.length = 1;
            ++ch;
            break;
        case '.':
            token.type = CflTokenType_Dot;
            token.length = 1;
            ++ch;
            break;
        default:
            if (cfl_is_whitespace(*ch))
            {
                token.type = CflTokenType_Whitespaces;
                const char* begin = ch;
                do
                {
                    ++ch;
                } while ((*ch != '\0') && cfl_is_whitespace(*ch));
                token.length = (int)(ch - begin);
            }
            else if (cfl_is_alphabet(*ch))
            {
                token.type = CflTokenType_Chars;
                const char* begin = ch;
                do
                {
                    ++ch;
                } while ((*ch != '\0') && cfl_is_alphabet(*ch));
                token.length = (int)(ch - begin);
            }
            else if (cfl_is_number(*ch))
            {
                token.type = CflTokenType_Numbers;
                const char* begin = ch;
                do
                {
                    ++ch;
                } while ((*ch != '\0') && cfl_is_number(*ch));
                token.length = (int)(ch - begin);
            }
        }

        cfl_token_buffer_add(&result, token);
        //cfl__log(token);
    }

    cfl_token_buffer_add(&result, (CflToken){.type = CflTokenType_EOF});
    return result;
}

/*
struct declaration

struct (name | null) { (chars | spaces | semicolon | null) } ;
*/

/*
keyword

chars
*/

bool cfl_token_matches_string(const CflToken* input, const char* str)
{
    if (input->type != CflTokenType_Chars)
        return false;

    int len = (int)strlen(str);
    if (input->length == len && (strncmp(input->begin, str, len) == 0))
        return true;

    return false;
}

typedef enum
{
    CflExpression_StructDeclaration,
    CflExpression_KeywordStruct,
    CflExpression_Whitespace,
    CflExpression_Identifier,
    CflExpression_StructSubDeclarationList,
    CflExpression_MemberDeclaration,
    CflExpression_TypeSpecifier,
    CflExpression_OpenBrace,
    CflExpression_ClosedBrace,
    CflExpression_Semicolon,
} CflExpression;

bool cfl_check_expression(CflToken* input, CflToken* end, CflToken** out_descent, CflExpression expression, bool optional)
{
    if (input == end)
        return optional;

    switch (expression)
    {
    case CflExpression_StructDeclaration:
    {
        CflToken* descent = input;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_KeywordStruct, false))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, false))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Identifier, false))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_OpenBrace, false))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_StructSubDeclarationList, true))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_ClosedBrace, false))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Semicolon, false))
            return optional;
        *out_descent = descent;
    } break;
    case CflExpression_KeywordStruct:
        if (!cfl_token_matches_string(input, "struct"))
            return optional;
        *out_descent = input + 1;
        break;
    case CflExpression_Whitespace:
        if (input->type != CflTokenType_Whitespaces)
            return optional;
        *out_descent = input + 1;
        break;
    case CflExpression_Identifier:
        if (input->type != CflTokenType_Chars)
            return optional;

        if (!(cfl_is_alphabet(*input->begin) || *input->begin == '_'))
            return optional;

        const char* ch = input->begin;
        for (int i = 0; i < input->length; ++i)
        {
            if (!(cfl_is_alphabet(*ch) || cfl_is_number(*ch) || *ch == '_'))
                return optional;
        }
        *out_descent = input + 1;
        break;
    case CflExpression_StructSubDeclarationList:
    {
        CflToken* descent = input;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        bool has_declarations = false;
        while (descent != end)
        {
            if (!cfl_check_expression(descent, end, &descent, CflExpression_MemberDeclaration, false))
                break;
            else
                has_declarations = true;
        }
        if (!has_declarations)
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        *out_descent = descent;
    } break;
    case CflExpression_MemberDeclaration:
    {
        CflToken* descent = input;
        if (!cfl_check_expression(input, end, &descent, CflExpression_Whitespace, true))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_TypeSpecifier, false))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Identifier, false))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Semicolon, false))
            return optional;
        if (!cfl_check_expression(descent, end, &descent, CflExpression_Whitespace, true))
            return optional;
        *out_descent = descent;
    } break;
    case CflExpression_TypeSpecifier:
    {
        if (input->type != CflTokenType_Chars)
            return optional;

        const char* type_names[] = {
            "char",
            "int",
            "float",
            "double",
        };

        bool is_type_specifier = false;
        for (int i = 0; i < cfl_array_count(type_names); ++i)
        {
            if (cfl_token_matches_string(input, type_names[i]))
            {
                is_type_specifier = true;
                break;
            }
        }

        if (!is_type_specifier)
            return optional;

        *out_descent = input + 1;
    } break;
    case CflExpression_OpenBrace:
        if (input->type != CflTokenType_OpenBrace)
            return optional;
        *out_descent = input + 1;
        break;
    case CflExpression_ClosedBrace:
        if (input->type != CflTokenType_ClosedBrace)
            return optional;
        *out_descent = input + 1;
        break;
    case CflExpression_Semicolon:
        if (input->type != CflTokenType_Semicolon)
            return optional;
        *out_descent = input + 1;
        break;
    default:
        cfl_assert(false);
    }

    return true;
}


#define cfl_max_string_length 200
#define cfl_max_member_info_count 50

typedef struct
{
    char type_name[cfl_max_string_length];
    char var_name[cfl_max_string_length];
} CflMemberInfo;

typedef struct
{
    char name[cfl_max_string_length];
    CflMemberInfo member_infos[cfl_max_member_info_count];
    int member_count;
} CflStructInfo;

typedef struct
{
    CflStructInfo* struct_infos;
} CflStructInfoBuffer;

CflToken* cfl__eat_whitespaces(CflToken* token)
{
    while (token->type == CflTokenType_Whitespaces) { ++token; }
    return token;
}

CflStructInfo cfl__build_struct_info(CflToken* token, CflToken* end)
{
    token = cfl__eat_whitespaces(token);

    CflToken* struct_decl_end;
    {
        bool result = cfl_check_expression(
            token, end, &struct_decl_end, CflExpression_StructDeclaration, false);
        cfl_assert(result);
    }

    token = cfl__eat_whitespaces(token + 1);

    CflStructInfo info = {0};
    strncpy(info.name, token->begin, token->length);

    while (token != struct_decl_end)
    {
        token = cfl__eat_whitespaces(token);

        CflToken* next;
        if (cfl_check_expression(
            token, end, &next, CflExpression_MemberDeclaration, false))
        {
            strncpy(
                info.member_infos[info.member_count].type_name,
                token->begin, token->length);
            token = cfl__eat_whitespaces(token + 1);
            strncpy(
                info.member_infos[info.member_count].var_name,
                token->begin, token->length);
            ++info.member_count;
            token = next;
        }
        else
            ++token;
    }
    
    return info;
}

void cfl_make_ast(CflTokenBuffer token_buffer)
{
    CflToken* end = &token_buffer.tokens[cfl_token_buffer_size(&token_buffer) - 1];
    CflToken* token = token_buffer.tokens;

    while (token != end)
    {
        CflToken* next;
        if (cfl_check_expression(token, end, &next, CflExpression_StructDeclaration, false))
        {
            CflStructInfo test = cfl__build_struct_info(token, end);
            printf("Found struct declaration %s\n", test.name);
            printf("struct %s\n{\n", test.name);
            for (int i = 0; i < test.member_count; ++i)
            {
                CflMemberInfo* member_info = &test.member_infos[i];
                printf("    %s %s;\n", member_info->type_name, member_info->var_name);
            }
            printf("};\n");
            token = next;
        }
        else
            ++token;
    }
}

#endif//CFLECTION_H
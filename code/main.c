#include "clfection.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE* file = fopen("sample.c.txt", "r");
    fseek(file, 0, SEEK_END);
    int file_length = ftell(file);
    fseek(file, 0, SEEK_SET);
    char* buffer = malloc(file_length);
    int read_length = (int)fread(buffer, sizeof(*buffer), file_length, file);
    buffer[read_length] = '\0';
    fclose(file);

    CflTokenBuffer token_buffer = cfl_tokenize(buffer);
    cfl_make_ast(token_buffer);


    cfl_token_buffer_free(&token_buffer);
    free(buffer);

    return 0;
}